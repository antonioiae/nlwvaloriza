import { Router } from "express";
import { TagController } from "./controllers/TagController";
import { UserController } from "./controllers/UserController";
import { AuthenticateUserController } from "./controllers/AuthenticateUserController";
import { ComplimentController } from "./controllers/ComplimentController";
import { ensureAdmin } from "./middlewares/ensureAdmin";
import { ensureAuthenticated } from "./middlewares/ensureAuthenticated";

const router = Router();

const userController = new UserController();
const tagController = new TagController();
const complimentController = new ComplimentController();
const authenticateUserController = new AuthenticateUserController();

router.post("/login", authenticateUserController.handle);

router.post("/tags", ensureAuthenticated, ensureAdmin, tagController.save);
router.get("/tags", ensureAuthenticated, ensureAdmin, tagController.getTags);

router.post("/compliments", ensureAuthenticated, ensureAdmin, complimentController.save);

router.post("/users", userController.save);
router.get("/users", userController.getAll);
router.get("/users/compliments/sent", ensureAuthenticated, userController.getComplimentsSent);
router.get("/users/compliments/received", ensureAuthenticated, userController.getComplimentsReceived);

export { router };
