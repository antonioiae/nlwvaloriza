import { Request, Response } from "express";
import { ComplimentService } from "../services/ComplimentService";

class ComplimentController {
    async save(request: Request, response: Response) {
        const { tag_id, user_receiver, message } = request.body;
        const { user_id } = request;
        const complimentService = new ComplimentService();
        const compliment = await complimentService.save({
            tag_id,
            user_sender: user_id,
            user_receiver,
            message
        });
        response.json(compliment);
    }
}

export { ComplimentController }