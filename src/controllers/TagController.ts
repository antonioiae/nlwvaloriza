import { Request, Response } from "express";
import { TagService } from "../services/TagService";

class TagController {
    async save(request: Request, response: Response) {
        const tagService = new TagService();
        const tag = await tagService.createTag(request.body);
        return response.json(tag);
    }

    async getTags(request: Request, response: Response) {
        const tagService = new TagService();
        let tags = await tagService.getTags();
        return response.json(tags);
    }
}

export { TagController };
