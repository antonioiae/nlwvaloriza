import { Request, Response } from "express";
import { UserService } from "../services/UserService";

class UserController {
    async save(request: Request, response: Response) {
        const userService = new UserService();
        const user = await userService.createUser(request.body);
        return response.json(user);
    }

    async getAll(request: Request, response: Response) {
        const userService = new UserService();
        const users = await userService.getUsers();
        return response.json(users);
    }

    async getComplimentsReceived(request: Request, response: Response) {
        const userService = new UserService();
        let compliments = await userService.getComplimentsReceived(request.user_id);
        return response.json(compliments);
    }

    async getComplimentsSent(request: Request, response: Response) {
        const userService = new UserService();
        let compliments = await userService.getComplimentsSent(request.user_id);
        return response.json(compliments);
    }
}

export { UserController };
