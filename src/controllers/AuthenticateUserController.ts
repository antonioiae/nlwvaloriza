import { Request, Response } from "express";
import { AuthenticateUserService } from "../services/AuthenticateUserService";

class AuthenticateUserController {
    async handle(request: Request, response: Response) {
        const authenticateUserService = new AuthenticateUserService();
        const token = await authenticateUserService.execute(request.body);
        return response.json(token);
    }
}

export { AuthenticateUserController }