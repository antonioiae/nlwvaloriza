import { getCustomRepository } from "typeorm";
import { TagRepository } from "../repositories/TagRepository";
import { classToPlain } from "class-transformer";

interface ITagRequest {
    name: string;
}

class TagService {
    async createTag({ name }: ITagRequest) {
        const tagRepository = getCustomRepository(TagRepository);
        if (!name) {
            throw new Error("Incorrect Name!");
        }
        const tagAlreadyExists = await tagRepository.findOne({ name });
        if (tagAlreadyExists) {
            throw new Error("Tag already exists!");
        }
        const tag = tagRepository.create({ name });
        await tagRepository.save(tag);
        return tag;
    }
    async getTags() {
        const tagRepository = getCustomRepository(TagRepository);
        return classToPlain(await tagRepository.find());
    }
}

export { TagService };
