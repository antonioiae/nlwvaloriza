import { getCustomRepository } from "typeorm";
import { UserRepository } from "../repositories/UserRepository";
import { hash } from "bcryptjs";
import { ComplimentRepository } from "../repositories/ComplimentRepository";
import { classToPlain } from "class-transformer";

interface IUserRequest {
    name: string;
    email: string;
    admin?: boolean;
    password: string;
}

class UserService {
    async createUser({ name, email, password, admin = false }: IUserRequest) {
        const userRepository = getCustomRepository(UserRepository);
        if (!email) {
            throw new Error("Incorrect email!");
        }
        const userAlreadyExists = await userRepository.findOne({
            email,
        });
        if (userAlreadyExists) {
            throw new Error("User already exists!");
        }
        const passwordHash = await hash(password, 8);
        const user = userRepository.create({ name, email, password: passwordHash, admin, });
        await userRepository.save(user);
        return user;
    }

    async getUsers() {
        const userRepository = getCustomRepository(UserRepository);
        return classToPlain(await userRepository.find());
    }

    async getComplimentsReceived(user_id: string) {
        const complimentsRepository = getCustomRepository(ComplimentRepository);
        return await complimentsRepository.find({
            where: {
                user_receiver: user_id
            },
            relations: ["userSender", "userReceiver", "tag"]
        })
    }

    async getComplimentsSent(user_id: string) {
        const complimentsRepository = getCustomRepository(ComplimentRepository);
        return await complimentsRepository.find({
            where: {
                user_sender: user_id
            },
            relations: ["userSender", "userReceiver", "tag"]
        })
    }
}

export { UserService };
