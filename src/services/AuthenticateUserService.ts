import { getCustomRepository } from "typeorm";
import { UserRepository } from "../repositories/UserRepository";
import { compare } from "bcryptjs";
import { sign } from "jsonwebtoken";

interface IAuthenticateRequeste {
    email: string;
    password: string;
}

class AuthenticateUserService {
    async execute({ email, password }: IAuthenticateRequeste) {
        const userRepository = getCustomRepository(UserRepository);
        const user = await userRepository.findOne({
            email
        })
        if (!user) {
            throw new Error("Email/password incorrect");
        }
        const passwordMatch = await compare(password, user.password);
        if (!passwordMatch) {
            throw new Error("Email/password incorrect");
        }
        return sign({ email: user.email }, "d2f6a0d0f9ee4adff8ee8ac95555dcec", {
            subject: user.id,
            expiresIn: "1d"
        });
    }
}

export { AuthenticateUserService }