import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";

interface IPayload {
    sub: string;
}

export function ensureAuthenticated(request: Request, response: Response, next: NextFunction) {
    const authToken = request.headers.authorization;
    if (!authToken) {
        return response.status(401).end();
    }
    const [, token] = authToken.split(' ');
    try {
        const { sub } = verify(token, 'd2f6a0d0f9ee4adff8ee8ac95555dcec') as IPayload;
        request.user_id = sub;
        return next();
    } catch (e) {
        return response.status(401).end();
    }
}